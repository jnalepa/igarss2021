# Towards Robust Cloud Detection in Satellite Images Using U-Nets

**Bartosz Grabowski, Maciej Ziaja, Michal Kawulok and Jakub Nalepa**

(Submitted to IGARSS 2021)

## Test scenes from the Landsat 8 Cloud Cover Assessment Validation Data

The following scenes from the Landsat 8 Cloud Cover Assessment Validation Data (https://landsat.usgs.gov/landsat-8-cloud-cover-assessment-validation-data) were chosen to our test set:

- **Barren:**
LC81640502013179LGN01 and
LC81390292014135LGN00

- **Forest:**
LC80160502014041LGN00 and
LC81720192013331LGN00
 
- **Grass/Crops:**
LC82020522013141LGN01 and 
LC81220422014096LGN00

- **Shrubland:**
LC80750172013163LGN00 and 
LC80670172014206LGN00

- **Snow/Ice:**
LC82271192014287LGN00 and 
LC81321192014054LGN00

- **Urban:**
LC80460282014171LGN00 and 
LC81940222013245LGN00
 
- **Water:**
LC81910182013240LGN00 and 
LC82150712013152LGN00

- **Wetlands:**
LC81010142014189LGN00 and 
LC81500152013225LGN00

